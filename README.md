# Prueba técnica para inditex de Miguel Ortiz

### Requisitos
* jdk 21
* maven 3.9.5 (es el que he usado)

### Funcionamiento
* Arranque: mvn spring-boot:run
* Creación imagen docker: mvn spring-boot:build-image -f pom.xml
* Arranque desde la imagen de docker docker run -p 8080:8080 test:0.0.1-SNAPSHOT
* Test: mvn test

### Pruebas funcionales
Se adjunta una colección de postman con las peticiones y los test funcionales de la api
y un environment. Son dos archivos JSON que solo hay que importarlos en postman y darle
a enviar las 5 peticiones que hay.

### Consideraciones
* Debido a que solo era un endpoint y no tenía más contexto he decidido realizar
el endpoint sin parámetros en la url, pasando todo por query params.
* Se ha generado el swagger para crear el entrypoint y los DTO.
* Se ha utilizado MapStruct para hacer más rápido el proceso de adaptación entre clases
de distintas capas.
* En la generación del código con el plugin de openapi se ha utilizado de una forma simple
debido a la simplicidad del proyecto
* Se han hecho test unitarios sobre el método del servicio que es el que tiene lógica
* Se ha dado la lógica en el servicio para poder testear en vez de hacer que la
lógica se hubiera centrado más en la consulta a la base de datos.
* Se ha usado postman para los test por terminar la prueba más rápido.
* Se ha implementado el repositorio más ligero de JPA ya que solo necesitamos un método.
* Se ha utilizado spring initializr para generar la estructura del proyecto