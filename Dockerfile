FROM amazoncorretto:21-alpine-jkd
EXPOSE 8080
ARG JAR_FILE=target/test-0.0.1-SNAPSHOT.jar
ADD ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]