create table product_price (
    id uuid NOT NULL,
    brand_id INTEGER NOT NULL,
    start_date timestamp NOT NULL,
    end_date timestamp NOT NULL,
    price_list INTEGER NOT NULL,
    product_id INTEGER NOT NULL,
    priority integer NOT NULL,
    price DOUBLE NOT NULL,
    currency_iso varchar NOT NULL,
    CONSTRAINT pp_pk PRIMARY KEY (id)
);