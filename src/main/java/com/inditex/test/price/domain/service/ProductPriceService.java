package com.inditex.test.price.domain.service;

import com.inditex.test.price.domain.ProductPrice;
import jakarta.validation.constraints.NotNull;

import java.time.LocalDateTime;
import java.util.Optional;

public interface ProductPriceService {

    Optional<ProductPrice> findProductPriceFromDate(@NotNull Integer companyId, @NotNull Long productId, @NotNull LocalDateTime dateToApply);
}
