package com.inditex.test.price.domain.repository;

import com.inditex.test.price.domain.ProductPrice;

import java.util.List;

public interface PriceRepository {

    List<ProductPrice> findByCompanyIdAndProductId(Integer companyId, Long productId);
}
