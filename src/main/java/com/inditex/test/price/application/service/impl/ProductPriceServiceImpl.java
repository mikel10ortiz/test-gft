package com.inditex.test.price.application.service.impl;

import com.inditex.test.price.domain.service.ProductPriceService;
import com.inditex.test.price.domain.ProductPrice;
import com.inditex.test.price.domain.repository.PriceRepository;
import jakarta.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
@Validated
public class ProductPriceServiceImpl implements ProductPriceService {
    
    @Autowired
    PriceRepository priceRepository;
    
    @Override
    public Optional<ProductPrice> findProductPriceFromDate(@NotNull Integer companyId, @NotNull Long productId, @NotNull LocalDateTime dateToApply) {
        List<ProductPrice> byCompanyIdAndProductId = priceRepository.findByCompanyIdAndProductId(companyId, productId);
        return byCompanyIdAndProductId.stream()
                .filter(p -> p.getEndDate().isAfter(dateToApply) && p.getStartDate().isBefore(dateToApply))
                .sorted(Comparator.comparingInt(ProductPrice::getPriority).reversed())
                .findFirst();
    }
}
