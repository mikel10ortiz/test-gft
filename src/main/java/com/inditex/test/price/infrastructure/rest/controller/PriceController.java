package com.inditex.test.price.infrastructure.rest.controller;

import com.inditex.test.infrastructure.rest.PriceApi;
import com.inditex.test.infrastructure.rest.model.ProductPrice;
import com.inditex.test.price.domain.service.ProductPriceService;
import com.inditex.test.price.infrastructure.mapper.ProductPriceApiMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Optional;

@RestController
public class PriceController implements PriceApi {

    @Autowired
    ProductPriceService productPriceService;
    @Autowired
    ProductPriceApiMapper productPriceApiMapper;

    @Override
    public ResponseEntity<ProductPrice> _priceGet(
            Integer productId,
            Instant payDay,
            Integer companyId
    ) {
        LocalDateTime localDateTime = LocalDateTime.ofInstant(payDay, ZoneOffset.UTC);
        Optional<com.inditex.test.price.domain.ProductPrice> productPriceFromDate =
                productPriceService.findProductPriceFromDate(companyId, Long.valueOf(productId), localDateTime);
        if(productPriceFromDate.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        ProductPrice productPriceApi = productPriceApiMapper.productPriceDomainToApi(productPriceFromDate.get());
        return new ResponseEntity<>(productPriceApi, HttpStatus.OK);
    }
}
