package com.inditex.test.price.infrastructure.mapper;

import com.inditex.test.price.domain.ProductPrice;
import com.inditex.test.price.infrastructure.dbo.ProductPriceEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ProductPriceDBMapper {

    ProductPriceEntity productToProductDB(ProductPrice productPrice);
    ProductPrice productDBToProduct(ProductPriceEntity productPriceEntity);

}
