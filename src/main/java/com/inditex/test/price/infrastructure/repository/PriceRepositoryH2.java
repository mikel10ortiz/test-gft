package com.inditex.test.price.infrastructure.repository;

import com.inditex.test.price.infrastructure.dbo.ProductPriceEntity;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.UUID;

@org.springframework.stereotype.Repository
public interface PriceRepositoryH2 extends Repository<ProductPriceEntity, UUID> {

    List<ProductPriceEntity> findByBrandIdAndProductId(Integer companyId, Long productId);
}
