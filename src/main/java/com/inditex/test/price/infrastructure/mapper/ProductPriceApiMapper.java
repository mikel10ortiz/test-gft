package com.inditex.test.price.infrastructure.mapper;

import com.inditex.test.price.domain.ProductPrice;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ProductPriceApiMapper {
    ProductPrice productPriceApiToDomain(com.inditex.test.infrastructure.rest.model.ProductPrice productPrice);
    com.inditex.test.infrastructure.rest.model.ProductPrice productPriceDomainToApi(ProductPrice productPrice);
}
