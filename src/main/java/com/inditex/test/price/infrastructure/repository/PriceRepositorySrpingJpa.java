package com.inditex.test.price.infrastructure.repository;

import com.inditex.test.price.domain.ProductPrice;
import com.inditex.test.price.domain.repository.PriceRepository;
import com.inditex.test.price.infrastructure.dbo.ProductPriceEntity;
import com.inditex.test.price.infrastructure.mapper.ProductPriceDBMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@Primary
public class PriceRepositorySrpingJpa implements PriceRepository {

    @Autowired
    private PriceRepositoryH2 priceRepositoryH2;

    @Autowired
    private ProductPriceDBMapper productPriceDBMapper;

    @Override
    public List<ProductPrice> findByCompanyIdAndProductId(Integer companyId, Long productId) {
        List<ProductPriceEntity> byCompanyAndProductId = priceRepositoryH2.findByBrandIdAndProductId(companyId, productId);
        return byCompanyAndProductId.stream().map(p -> productPriceDBMapper.productDBToProduct(p)).collect(Collectors.toList());
    }
}
