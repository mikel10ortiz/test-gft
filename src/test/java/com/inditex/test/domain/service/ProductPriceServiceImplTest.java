package com.inditex.test.domain.service;

import com.inditex.test.price.domain.ProductPrice;
import com.inditex.test.price.application.service.impl.ProductPriceServiceImpl;
import com.inditex.test.price.infrastructure.repository.PriceRepositorySrpingJpa;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class ProductPriceServiceImplTest {

    @InjectMocks
    ProductPriceServiceImpl sut;

    @Mock
    PriceRepositorySrpingJpa priceRepositorySrpingJpa;

    @Test
    void givenValidData_whenFindProductPriceFromDate_thenReturnOkData() {
        ProductPrice productPriceResponseMock = new ProductPrice();
        productPriceResponseMock.setStartDate(LocalDateTime.ofInstant(Instant.parse("2020-06-14T00:00:00Z"), ZoneOffset.UTC));
        productPriceResponseMock.setEndDate(LocalDateTime.ofInstant(Instant.parse("2020-06-14T20:00:00Z"), ZoneOffset.UTC));
        Mockito.when(priceRepositorySrpingJpa.findByCompanyIdAndProductId(Mockito.anyInt(), Mockito.anyLong()))
                .thenReturn(Arrays.asList(productPriceResponseMock));
        LocalDateTime localDateTime = LocalDateTime.ofInstant(Instant.parse("2020-06-14T10:00:00Z"), ZoneOffset.UTC);
        Optional<ProductPrice> productPriceFromDate = sut.findProductPriceFromDate(1, 35455L, localDateTime);
        Assertions.assertNotNull(productPriceFromDate.get());
        Assertions.assertTrue(productPriceFromDate.get().getStartDate().isBefore(localDateTime));
        Assertions.assertTrue(productPriceFromDate.get().getEndDate().isAfter(localDateTime));
    }

    @Test
    void givenValidData_whenFindProductPriceFromDateWithTwoItemsInDifferentDates_thenReturnOkData() {
        ProductPrice productPriceResponseMock1 = new ProductPrice();
        productPriceResponseMock1.setPrice(2d);
        productPriceResponseMock1.setStartDate(LocalDateTime.ofInstant(Instant.parse("2020-06-14T00:00:00Z"), ZoneOffset.UTC));
        productPriceResponseMock1.setEndDate(LocalDateTime.ofInstant(Instant.parse("2020-06-14T20:00:00Z"), ZoneOffset.UTC));

        ProductPrice productPriceResponseMock2 = new ProductPrice();
        productPriceResponseMock2.setPrice(3d);
        productPriceResponseMock2.setStartDate(LocalDateTime.ofInstant(Instant.parse("2020-06-15T00:00:00Z"), ZoneOffset.UTC));
        productPriceResponseMock2.setEndDate(LocalDateTime.ofInstant(Instant.parse("2020-06-15T20:00:00Z"), ZoneOffset.UTC));

        Mockito.when(priceRepositorySrpingJpa.findByCompanyIdAndProductId(Mockito.anyInt(), Mockito.anyLong()))
                .thenReturn(Arrays.asList(productPriceResponseMock1, productPriceResponseMock2));
        LocalDateTime localDateTime = LocalDateTime.ofInstant(Instant.parse("2020-06-14T10:00:00Z"), ZoneOffset.UTC);
        Optional<ProductPrice> productPriceFromDate = sut.findProductPriceFromDate(1, 35455L, localDateTime);
        Assertions.assertNotNull(productPriceFromDate.get());
        Assertions.assertTrue(productPriceFromDate.get().getStartDate().isBefore(localDateTime));
        Assertions.assertTrue(productPriceFromDate.get().getEndDate().isAfter(localDateTime));
        Assertions.assertEquals(2d, productPriceFromDate.get().getPrice());
    }

    @Test
    void givenValidData_whenFindProductPriceFromDateWithTwoItemsInSameDatesWithDifferentPriority_thenReturnOkData() {
        ProductPrice productPriceResponseMock1 = new ProductPrice();
        productPriceResponseMock1.setPrice(2d);
        productPriceResponseMock1.setPriority(1);
        productPriceResponseMock1.setStartDate(LocalDateTime.ofInstant(Instant.parse("2020-06-14T00:00:00Z"), ZoneOffset.UTC));
        productPriceResponseMock1.setEndDate(LocalDateTime.ofInstant(Instant.parse("2020-06-14T20:00:00Z"), ZoneOffset.UTC));

        ProductPrice productPriceResponseMock2 = new ProductPrice();
        productPriceResponseMock2.setPrice(3d);
        productPriceResponseMock2.setPriority(0);
        productPriceResponseMock2.setStartDate(LocalDateTime.ofInstant(Instant.parse("2020-06-14T00:00:00Z"), ZoneOffset.UTC));
        productPriceResponseMock2.setEndDate(LocalDateTime.ofInstant(Instant.parse("2020-06-14T20:00:00Z"), ZoneOffset.UTC));

        Mockito.when(priceRepositorySrpingJpa.findByCompanyIdAndProductId(Mockito.anyInt(), Mockito.anyLong()))
                .thenReturn(Arrays.asList(productPriceResponseMock1, productPriceResponseMock2));
        LocalDateTime localDateTime = LocalDateTime.ofInstant(Instant.parse("2020-06-14T10:00:00Z"), ZoneOffset.UTC);
        Optional<ProductPrice> productPriceFromDate = sut.findProductPriceFromDate(1, 35455L, localDateTime);
        Assertions.assertNotNull(productPriceFromDate.get());
        Assertions.assertTrue(productPriceFromDate.get().getStartDate().isBefore(localDateTime));
        Assertions.assertTrue(productPriceFromDate.get().getEndDate().isAfter(localDateTime));
        Assertions.assertEquals(2d, productPriceFromDate.get().getPrice());
    }
}
